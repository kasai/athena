template<class CONTAINER>
std::vector< TrigCompositeUtils::LinkInfo<CONTAINER> > Trig::ChainGroup::features(EventPtr_t eventStore,
        unsigned int condition, const std::string /*container*/, 
        const unsigned int featureCollectionMode, const std::string featureName) const {

  // TODO impliment checking of container name

  // Proper adherence to the condition bits in Run 3 is to follow.
  bool errState = false;
  if ( condition != TrigDefs::Physics && condition != TrigDefs::includeFailedDecisions ) {
    ATH_MSG_ERROR("features may only be called with: "
      "TrigDefs::Physics - features from the legs of the chain(s) which passed the trigger. "
      "TrigDefs::includeFailedDecisions - all features from the chain(s) irrespective of pass/fail of each Step.");
    errState = true;
  }

  if ( featureCollectionMode != TrigDefs::oneFeaturePerLeg && featureCollectionMode != TrigDefs::allFeaturesPerLeg ) {
    ATH_MSG_ERROR("featureCollectionMode may only be called with: "
      "TrigDefs::oneFeaturePerLeg - stop exploring each route through the navigation once a feature matching all requirements is found. "
      "TrigDefs::allFeaturesPerLeg - always fully explore each route throught the navigation graph and collect all matching features.");
    errState = true;
  }

  // TODO when we decide what happens to CacheGlobalMemory - this needs to be updated to use a ReadHandle
  const TrigCompositeUtils::DecisionContainer* navigationSummaryContainer = nullptr;
  if (eventStore->retrieve(navigationSummaryContainer, "HLTSummary").isFailure() || navigationSummaryContainer == nullptr) {
    ATH_MSG_ERROR("Unable to read Run 3 trigger navigation. Cannot retrieve features.");
    errState = true;
  }

  // We always want to search from the passed raw terminus node to find features for passed chains.
  const TrigCompositeUtils::Decision* terminusNode = nullptr;
  if (!errState) {
    for (const TrigCompositeUtils::Decision* decision : *navigationSummaryContainer) {
      if (decision->name() == "HLTPassRaw") {
        terminusNode = decision;
        break;
      }
    }
    if (terminusNode == nullptr) {
      ATH_MSG_ERROR("Unable to locate HLTPassRaw element of HLTSummary");
      errState = true;
    }
  }

  if (errState) {
    ATH_MSG_ERROR("Encountered one or more errors in Trig::ChainGroup::features. Returning empty vector.");
    return std::vector< TrigCompositeUtils::LinkInfo<CONTAINER> >();
  }

  // For each chain, collect Navigation information
  std::vector< ElementLinkVector<TrigCompositeUtils::DecisionContainer> > allLinearNavigationPaths;

  // Loop over HLT chains
  std::set<const TrigConf::HLTChain*>::const_iterator chIt;
  for (chIt=conf_chain_begin(); chIt != conf_chain_end(); ++chIt) {
    const HLT::Chain* fchain = cgm()->chain(**chIt);
    if (fchain) {

      // Obtain navigation routes for passed chains
      // Final parameter TRUE as the chain passed (has its ID in terminus node)
      TrigCompositeUtils::recursiveGetDecisions(terminusNode, allLinearNavigationPaths, fchain->getChainHashId(), true);

      ATH_MSG_DEBUG("Added all passed navigation paths for chain " << fchain->getChainName() 
        << ", total paths:" << allLinearNavigationPaths.size());

      if (condition == TrigDefs::includeFailedDecisions) {
        std::vector<const TrigCompositeUtils::Decision*> rejectedDecisionNodes = 
          TrigCompositeUtils::getRejectedDecisionNodes(eventStore, fchain->getChainHashId());

        ATH_MSG_DEBUG("Chain " << fchain->getChainName() << " has " << rejectedDecisionNodes.size() 
          << " dangling nodes in the graph from objects which were rejected.");

        for (const TrigCompositeUtils::Decision* rejectedNode : rejectedDecisionNodes) {
          // Final parameter FALSE as the chain failed here (its ID was removed from rejectedNode)
          TrigCompositeUtils::recursiveGetDecisions(rejectedNode, allLinearNavigationPaths, fchain->getChainHashId(), false);
        }

        ATH_MSG_DEBUG("Added all failed navigation paths for chain " << fchain->getChainName() 
          << ", total paths:" << allLinearNavigationPaths.size());
      }

    } else {
      ATH_MSG_ERROR("Cannot access configuration for one of the ChainGroup's chains");
    }
  }

  if (allLinearNavigationPaths.size() == 0) {
    ATH_MSG_WARNING("No navigation paths found for this chain group of " << names().size() << " chains.");
  }

  const bool oneFeaturePerLeg = (featureCollectionMode == TrigDefs::oneFeaturePerLeg);
  return TrigCompositeUtils::getFeaturesOfType<CONTAINER>(allLinearNavigationPaths, oneFeaturePerLeg, featureName);
}
