#include "../SCTTracksMonTool.h"
#include "../SCTLorentzMonTool.h"
#include "../SCTRatioNoiseMonTool.h"
#include "../SCTErrMonTool.h"
#include "../SCTHitEffMonTool.h"
#include "../SCTHitsNoiseMonTool.h"
#include "../SCTLorentzMonAlg.h"

using namespace SCT_Monitoring;

DECLARE_COMPONENT( SCTTracksMonTool )
DECLARE_COMPONENT( SCTLorentzMonTool )
DECLARE_COMPONENT( SCTRatioNoiseMonTool )
DECLARE_COMPONENT( SCTErrMonTool )
DECLARE_COMPONENT( SCTHitEffMonTool )
DECLARE_COMPONENT( SCTHitsNoiseMonTool )
DECLARE_COMPONENT( SCTLorentzMonAlg )
