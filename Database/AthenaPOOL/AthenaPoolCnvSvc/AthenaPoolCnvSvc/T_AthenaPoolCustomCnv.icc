/*
  Copyright (C) 2002-2019 CERN for the benefit of the ATLAS collaboration
*/

/** @file T_AthenaPoolCustomCnv.icc
 *  @brief This file contains the implementation for the templated T_AthenaPoolCustomCnv class.
 *  @author Marcin.Nowak@cern.ch
 **/

#include "GaudiKernel/StatusCode.h"
#include "GaudiKernel/DataObject.h"
#include "GaudiKernel/IOpaqueAddress.h"
#include "GaudiKernel/IRegistry.h"

#include "StorageSvc/DbReflex.h"

#include "AthenaKernel/StorableConversions.h"

#include "AthenaPoolCnvTPExtension.h"

#include <stdexcept>

template <class TRANS, class PERS>
T_AthenaPoolCustomCnv<TRANS, PERS>::T_AthenaPoolCustomCnv(ISvcLocator* pSvcLocator,
                                                          const char* name /*= nullptr*/) : BaseType(pSvcLocator, name) {
}

template <class TRANS, class PERS>
StatusCode T_AthenaPoolCustomCnv<TRANS, PERS>::DataObjectToPers(DataObject* pObj, const std::string&/* key*/) {
   ATH_MSG_VERBOSE("In DataObjectToPers() for key = " << pObj->name());
   return(StatusCode::SUCCESS);
}

template <class TRANS, class PERS>
StatusCode T_AthenaPoolCustomCnv<TRANS, PERS>::DataObjectToPool(DataObject* pObj, const std::string& key) {
   ATH_MSG_VERBOSE("In DataObjectToPool() for key = " << pObj->name());
   TRANS* obj = nullptr;
   PERS* persObj = nullptr;
   bool success = SG::fromStorable(pObj, obj);
   if (!success || obj == nullptr) {
      ATH_MSG_ERROR("Failed to cast DataObject to transient type");
      return(StatusCode::FAILURE);
   }
   try {
      persObj = createPersistent(obj);
   } catch (std::exception &e) {
      ATH_MSG_ERROR("Failed to create persistent object: " << e.what());
      return(StatusCode::FAILURE);
   }
   m_persObjList.push_back(persObj);
   Token* token = nullptr;
   StatusCode status = this->objectToPool(persObj, token, key);
   // Null/empty token means ERROR
   if (token == nullptr || token->classID() == Guid::null()) {
      ATH_MSG_ERROR("failed to get Token for " << pObj->name());
      return(StatusCode::FAILURE);
   }
   // Update IOpaqueAddress for this object.
   TokenAddress* tokAddr = dynamic_cast<TokenAddress*>(pObj->registry()->address());
   if (tokAddr != nullptr) {
      tokAddr->setToken(token); token = nullptr; // Token will be inserted into DataHeader, which takes ownership
   } else { // No address (e.g. satellite DataHeader), delete Token
      delete token; token = nullptr;
      return(StatusCode::FAILURE);
   }
   return(status);
}

template <class TRANS, class PERS>
StatusCode T_AthenaPoolCustomCnv<TRANS, PERS>::PoolToDataObject(DataObject*& pObj, const Token* token) {
   if (token != nullptr) {
      this->m_classID = token->classID();
   }
   TRANS* transObj = nullptr;
   AthenaConverterTLPExtension *extCnv = dynamic_cast<AthenaConverterTLPExtension*>(this);
   // reset the TP converter used for reading, so old value is not used
   if (extCnv != nullptr) {
      extCnv->resetTPCnvForReading();
   }
   try {
      transObj = createTransient();
   } catch(std::exception &e) {
      ATH_MSG_ERROR("Failed to convert persistent object to transient: " << e.what());
      // cleanup
      if (extCnv != nullptr) {
         extCnv->deletePersistentObjects();
      }
      return(StatusCode::FAILURE);
   }
   if (extCnv != nullptr) {
      extCnv->deletePersistentObjects();
   }
   pObj = SG::asStorable(transObj);
   return(StatusCode::SUCCESS);
}

// Read object of type P.  This is an exception-throwing version of poolToObject()
// plus reading of all extending objects
template <class TRANS, class PERS>
template <class P>
inline P* T_AthenaPoolCustomCnv<TRANS, PERS>::poolReadObject() {
   P* persObj = nullptr;
   if (this->poolToObject(this->m_i_poolToken, persObj).isFailure()) {
      std::string error("POOL read failed. Token = ");
      throw std::runtime_error(error + (this->m_i_poolToken != nullptr ? this->m_i_poolToken->toString() : ""));
   }
   AthenaConverterTLPExtension *extCnv = dynamic_cast<AthenaConverterTLPExtension*>(this);
   if (extCnv != nullptr) {
      extCnv->readExtendingObjects(persObj);
   }
   return(persObj);
}

// Read object of type P (plus all extending objects)
// using the indicated top-level TP converter
template <class TRANS, class PERS>
template <class P>
inline void T_AthenaPoolCustomCnv<TRANS, PERS>::poolReadObject(TopLevelTPCnvBase& tlp_converter) {
   AthenaPoolCnvTPExtension *extCnv = dynamic_cast<AthenaPoolCnvTPExtension*>(this);
   // set which Top level TP concerter will by used for reading
   // only matters for extended converters
   if (extCnv != nullptr) {
      extCnv->usingTPCnvForReading(tlp_converter);
   }
   // read the object
   P* persObj = poolReadObject<P>();
   // remember the object we just read
   tlp_converter.setTLPersObject(persObj);
   // not returning the object - the TLP converter owns it now
   // and it will delete it automatically in createTransient()!
}

// Remember the POOL object until commit (then delete it)
template <class TRANS, class PERS>
inline void T_AthenaPoolCustomCnv<TRANS, PERS>::keepPoolObj(PERS* obj) {
   m_persObjList.push_back(obj);
}

// callback from the CleanupSvc to delete persistent object in the local list
template <class TRANS, class PERS>
StatusCode T_AthenaPoolCustomCnv<TRANS, PERS>::cleanUp() {
   for (typename std::vector<PERS*>::const_iterator i = m_persObjList.begin(); i != m_persObjList.end(); i++) {
      delete *i;
   }
   m_persObjList.clear();
   return(StatusCode::SUCCESS);
}
