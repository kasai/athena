#include "CoolLumiUtilities/FillParamsTool.h"
#include "CoolLumiUtilities/BunchGroupTool.h"
#include "CoolLumiUtilities/BunchLumisTool.h"
#include "CoolLumiUtilities/OnlineLumiCalibrationTool.h"
#include "../OnlineLumiCalibrationCondAlg.h"
#include "../BunchGroupCondAlg.h"
#include "../BunchLumisCondAlg.h"
#include "../FillParamsCondAlg.h"

DECLARE_COMPONENT( FillParamsTool )
DECLARE_COMPONENT( BunchGroupTool )
DECLARE_COMPONENT( BunchLumisTool )
DECLARE_COMPONENT( OnlineLumiCalibrationTool )
DECLARE_COMPONENT( OnlineLumiCalibrationCondAlg )
DECLARE_COMPONENT( BunchGroupCondAlg )
DECLARE_COMPONENT( BunchLumisCondAlg )
DECLARE_COMPONENT( FillParamsCondAlg )

